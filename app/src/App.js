import React, {Component} from 'react';
import Nume from './nume.js';
import Contacte from './contacte.js';

class App extends Component{
  constructor(){
    super();
    this.state = {
      parametru1: "Silviu1"
    }
  }

  render(){
    return (
      <div className="App">
        <Contacte valoare={this.state.parametru1}/>
        <Nume valoare={this.state.parametru1}/>
      </div>
    );
  }
}

export default App;
